#include "receiver.h"

#ifdef __cplusplus
extern "C" {
#endif

void receiverInit(receiver_t *self, message_t *message) {
    self->message = message;
    receiverReset(self);
}

void receiverProcess(receiver_t *self, uint8_t newByte) {
    uint8_t i;
    switch (self->state) {
        case receiverStateLookingForType:
            if (messageIsTypeValid(newByte)) {
                self->message->data[0] = newByte;
                self->state = receiverStateLookingForLength;
            }
            break;

        case receiverStateLookingForLength:
            if (newByte < 40) {
                self->message->data[1] = newByte;
                if (newByte == 0) {
                    self->state = receiverStateFinished;
                } else {
                    self->state = receiverStateReceivingPayload;
                    self->remainingBytes = newByte;
                }
            } else {
                self->state = receiverStateLookingForType;
                self->message->data[0] = cmdNone;
            }
            break;

        case receiverStateReceivingPayload:
            i = 2 + messageGetPayloadSize(self->message) - self->remainingBytes;
            self->message->data[i] = newByte;
            self->remainingBytes = self->remainingBytes - 1;

            if (self->remainingBytes == 0) {
                self->state = receiverStateFinished;
            }
            break;

        default:
            break;
    }
}

bool receiverHasNewMessage(receiver_t *self) {
    return self->state == receiverStateFinished;
}

void receiverReset(receiver_t *self) {
    self->state = receiverStateLookingForType;
    messageClear(self->message);
}

#ifdef __cplusplus
}
#endif
