#include "message.h"
#include <memory.h>

void messageClear(message_t* self)
{
    self->data[protocolFieldType] = (uint8_t) cmdNone;
    self->data[protocolFieldLength] = 0;
}

void messageCreate(message_t *self, messageType_t type)
{
    self->data[protocolFieldType] = type;
    self->data[protocolFieldLength] = 0;
}

uint8_t messageGetPayloadSize(message_t *self)
{
    return self->data[protocolFieldLength];
}

void messageAppendParameter(message_t *self, uint8_t data)
{
    self->data[messageGetTotalSize(self)] = data;
    self->data[protocolFieldLength] = self->data[protocolFieldLength] + 1;
}

void messageAppendParameters(message_t *self, const void *data, uint8_t size)
{
    memcpy(&self->data[messageGetTotalSize(self)], data, size);
    self->data[protocolFieldLength] = self->data[protocolFieldLength] + size;
}

uint8_t messageGetTotalSize(message_t *self)
{
    return messageGetPayloadSize(self) + protocolFieldPayload;
}

bool messageIsTypeValid(uint8_t type)
{
    return (type > ((uint8_t) cmdNone)) && (type < ((uint8_t) cmdSize));
}
