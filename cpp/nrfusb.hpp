#ifndef NRFUSB_HPP
#define NRFUSB_HPP

// Specific libraries for Windows systems
#ifdef WIN32
#include <SDKDDKVer.h>
#endif

#include <iostream>
#include <functional>
#include <memory>
#include <thread>
#include "nrfusb_definitions.hpp"

/**
 * Get current time (in milliseconds) since system initialization.
 * @return current time in milliseconds.
 */
uint64_t getTimeMs();

/**
 * Get current time (in microseconds) since system initialization.
 * @return current time in microseconds.
 */
uint64_t getTimeUs();

/**
 * Blocks the CPU during time_to_wait milliseconds.
 * @param time_to_wait
 */
void delayMs(uint64_t time_to_wait);

/**
 * Blocks the CPU during time_to_wait microseconds.
 * @param time_to_wait
 */
void delayUs(uint64_t time_to_wait);

/**
 * If the user wants to take advantage of interrupts, then it is required to implement a function and use it in the
 * Nrfusb instantiation. This function will receive three bool variables: tx_ok, tx_fail and rx_ready, exactly as
 * if the user ran whatHappened with the radio.
 */
using callback = std::function<void(bool, bool, bool)>;

struct NrfusbImpl;

std::shared_ptr<NrfusbImpl> NrfusbInit(const std::string& boardAddress, unsigned int timeoutMs=10, unsigned int maxRetries=2, std::function<void(bool, bool, bool)> userCallback=nullptr);
void NrfusbDestructor(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbIsChipConnected(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbStartListening(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbStopListening(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbAvailable(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbRead(const std::shared_ptr<NrfusbImpl>& self, void *buf, unsigned int len);
bool NrfusbWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len);
void NrfusbOpenWritingPipe(const std::shared_ptr<NrfusbImpl>& self, const uint8_t *address);
void NrfusbOpenReadingPipe(const std::shared_ptr<NrfusbImpl>& self, uint8_t pipe_number, const uint8_t* address);
bool NrfusbFailureDetected(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbAvailable(const std::shared_ptr<NrfusbImpl>& self, unsigned int* pipe_num);
bool NrfusbRxFifoFull(const std::shared_ptr<NrfusbImpl>& self);
uint8_t NrfusbIsFifo(const std::shared_ptr<NrfusbImpl>& self, bool about_tx);
bool NrfusbIsFifo(const std::shared_ptr<NrfusbImpl>& self, bool about_tx, bool check_empty);
void NrfusbPowerDown(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbPowerUp(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, bool multicast);
bool NrfusbWriteFast(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len);
bool NrfusbWriteFast(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, bool multicast);
bool NrfusbWriteBlocking(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, uint32_t timeout);
bool NrfusbTxStandBy(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbTxStandBy(const std::shared_ptr<NrfusbImpl>& self, uint32_t timeout, bool startTx);
bool NrfusbWriteAckPayload(const std::shared_ptr<NrfusbImpl>& self, unsigned int pipe, const void *buf, unsigned int len);
void NrfusbWhatHappened(const std::shared_ptr<NrfusbImpl>& self, bool &tx_ok, bool &tx_fail, bool &rx_ready);
void NrfusbStartFastWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, uint8_t len, bool multicast, bool startTx);
bool NrfusbStartWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, uint8_t len, bool multicast);
void NrfusbReUseTX(const std::shared_ptr<NrfusbImpl>& self);
unsigned int NrfusbFlush_tx(const std::shared_ptr<NrfusbImpl>& self);
unsigned int NrfusbFlush_rx(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbTestCarrier(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbTestRPD(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbIsValid(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbCloseReadingPipe(const std::shared_ptr<NrfusbImpl>& self, unsigned int pipe_number);
void NrfusbSetTxDelay(const std::shared_ptr<NrfusbImpl>& self, unsigned int txDelay);
unsigned int NrfusbGetTxDelay(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbSetCsDelay(const std::shared_ptr<NrfusbImpl>& self, unsigned int csDelay);
unsigned int NrfusbGetCsDelay(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbSetAddressWidth(const std::shared_ptr<NrfusbImpl>& self, unsigned int a_width);
void NrfusbSetRetries(const std::shared_ptr<NrfusbImpl>& self, unsigned int delay, unsigned int count);
void NrfusbSetChannel(const std::shared_ptr<NrfusbImpl>& self, unsigned int new_channel);
int NrfusbGetChannel(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbSetPayloadSize(const std::shared_ptr<NrfusbImpl>& self, unsigned int new_payload_size);
unsigned int NrfusbGetPayloadSize(const std::shared_ptr<NrfusbImpl>& self);
unsigned int NrfusbGetDynamicPayloadSize(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbEnableAckPayload(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbDisableAckPayload(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbEnableDynamicPayloads(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbDisableDynamicPayloads(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbEnableDynamicAck(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbIsPVariant(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbSetAutoAck(const std::shared_ptr<NrfusbImpl>& self, bool enable);
void NrfusbSetAutoAck(const std::shared_ptr<NrfusbImpl>& self, unsigned int pipe_number, bool enable);
void NrfusbSetPALevel(const std::shared_ptr<NrfusbImpl>& self, rf24_pa_dbm_e level, bool lnaEnable);
rf24_pa_dbm_e NrfusbGetPALevel(const std::shared_ptr<NrfusbImpl>& self);
unsigned int NrfusbGetARC(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbSetDataRate(const std::shared_ptr<NrfusbImpl>& self, rf24_datarate_e new_config);
rf24_datarate_e NrfusbGetDataRate(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbSetCRCLength(const std::shared_ptr<NrfusbImpl>& self, rf24_crclength_e length);
rf24_crclength_e NrfusbGetCRCLength(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbDisableCRC(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbMaskIRQ(const std::shared_ptr<NrfusbImpl>& self, bool tx_ok, bool tx_fail, bool rx_ready);
void NrfusbStartConstCarrier(const std::shared_ptr<NrfusbImpl>& self, rf24_pa_dbm_e level, unsigned int channel);
void NrfusbStopConstCarrier(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbToggleAllPipes(const std::shared_ptr<NrfusbImpl>& self, bool isEnabled);
void NrfusbSetRadiation(const std::shared_ptr<NrfusbImpl>& self, rf24_pa_dbm_e level, rf24_datarate_e speed, bool lnaEnable);
void NrfusbLedRxOn(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbLedRxOff(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbLedTxOn(const std::shared_ptr<NrfusbImpl>& self);
void NrfusbLedTxOff(const std::shared_ptr<NrfusbImpl>& self);
bool NrfusbComboWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, bool multicast);


/**
 * @brief The Nrfusb class represents the driver that should be used to control the Nrfusb board. It was built to allow
 *        that more than one board is used simultaneously.
 *
 * One of the main goals of this project is give to the user the same
 * experience provided by the https://github.com/nRF24/RF24 driver, i.e., by providing the same methods (and behaviour)
 * of that library, the community can enjoy all the knowledge already available.
 */
class Nrfusb {
public:
    /**
     * @name Driver initializers
     *
     *  These are the methods you need to initialize and de-initialize this driver.
     */
    /**@{*/

    /**
     * A complete constructor. It will call the begin method automatically.
     * @param boardAddress is the serial port path (COM in Windows or /dev/tty in Unix systems).
     * @param userCallback is a user defined function which will be automatically called when an interrupt is
     *                     triggered in the radio.
     */
    explicit Nrfusb(const std::string& boardAddress, std::function<void(bool, bool, bool)> userCallback=nullptr, const unsigned int timeoutMs=10, const unsigned int maxRetries=2)
    {
        self = NrfusbInit(boardAddress, timeoutMs, maxRetries, userCallback);
    }

    /**
     * Destructor.
     */
    ~Nrfusb()
    {
        NrfusbDestructor(self);
    }

    /**@}*/
    /**
     * @name Primary public interface
     *
     *  These are the main methods you need to operate the chip. These methods should have the same behaviour of the
     *  RF24 library. That's the reason why their documentation will follow RF24 closely.
     */
    /**@{*/

    /**
     * TODO
     * After the board is initialized, call this before calling any other methods. This method can also be used to
     * re-initialize the radio in case of failures.
     *
     * @code
     * if (!radio.begin()) {
     *   std::cout << "radio hardware not responding!" << std::endl;
     *   while (1) {} // hold program in infinite loop to prevent subsequent errors
     * }
     * @endcode
     * @return
     * - `true` if the radio was successfully initialized
     * - `false` if the MCU failed to communicate with the radio hardware
     */
    void begin();

    /**
     * Checks if the chip is connected to the SPI bus.
     * @return true if the chip is connected.
     */
    bool isChipConnected()
    {
        return NrfusbIsChipConnected(self);
    }

    /**
     * Start listening on the pipes opened for reading.
     *
     * 1. Be sure to call openReadingPipe() first.
     * 2. Do not call write() while in this mode, without first calling stopListening().
     * 3. Call available() to check for incoming traffic, and read() to get it.
     *
     * Open reading pipe 1 using address `0xCCCECCCECC`
     * @code
     * byte address[] = {0xCC, 0xCE, 0xCC, 0xCE, 0xCC};
     * radio.openReadingPipe(1,address);
     * radio.startListening();
     * @endcode
     *
     * @note If there was a call to openReadingPipe() about pipe 0 prior to
     * calling this function, then this function will re-write the address
     * that was last set to reading pipe 0. This is because openWritingPipe()
     * will overwrite the address to reading pipe 0 for proper auto-ack
     * functionality.
     */
    void startListening()
    {
        NrfusbStartListening(self);
    }

    /**
     * Stop listening for incoming messages, and switch to transmit mode.
     *
     * Do this before calling write():
     * @code
     * radio.stopListening();
     * radio.write(&data, sizeof(data));
     * @endcode
     *
     * @note When the ACK payloads feature is enabled, the TX FIFO buffers are
     * flushed when calling this function. This is meant to discard any ACK
     * payloads that were not appended to acknowledgment packets.
     */
    void stopListening()
    {
        NrfusbStopListening(self);
    }

    /**
     * Check whether there are bytes available to be read.
     *
     * @code
     * if(radio.available()){
     *   radio.read(&data,sizeof(data));
     * }
     * @endcode
     *
     * @see Nrfusb::available(unsigned int* pipe_num)
     *
     * @return true if there is a payload available, false if none is
     *
     * @note Differently from the RF24 documentation, the whatHappened method (unavailable in this driver) is
     *       automatically called by the board and therefore you do not need to worry about this detail.
     */
    bool available()
    {
        return NrfusbAvailable(self);
    }

    /**
     * Read payload data.
     *
     * @note Every time a RX_READY event is acknowledge by the Nrfusb board, it will automatically read the data from
     *       the radio and send it asynchronously to the computer driver. The Nrfusb::available or
     *       Nrfusb::read methods do not communicate with the board, the only read from the driver buffer. This ensures
     *       less usb traffic.
     *
     * The length of data read is usually the next available payload's length
     * @see
     * - Nrfusb::getPayloadSize()
     * - Nrfusb::getDynamicPayloadSize()
     *
     * @param buf Pointer to a buffer where the data should be written.
     * @param len Maximum number of bytes to read into the buffer. This
     * value should match the length of the object referenced using the
     * `buf` parameter. The absolute maximum number of bytes that can be read
     * in one call is 32 (for dynamic payload lengths) or whatever number was
     * previously passed to setPayloadSize() (for static payload lengths).
     *
     * Example code:
     *
     * @code
     * if(radio.available()) {
     *   radio.read(&data, sizeof(data));
     * }
     * @endcode
     */
    void read(void *buf, unsigned int len)
    {
        NrfusbRead(self, buf, len);
    }

    /**
     * This method blocks until the message is successfully acknowledged by the receiver or the timeout/retransmit
     * maxima are reached. In the default configuration, the max delay here is 60-70ms.
     *
     * @warning Be sure to call Nrfusb::openWritingPipe() first to set the destination of where to write to.
     *
     * The maximum size of data written is the fixed payload size, see
     * Nrfusb::getPayloadSize().  However, you can write less, and the remainder
     * will just be filled with zeroes.
     *
     * @note Every time a write method is called, the board will answer (through usb) with a response packet that
     *       contains the status of the write attempt (success or not), the arc count and the failure detect variable.
     *
     * @param buf Pointer to the data to be sent
     * @param len Number of bytes to be sent
     *
     * Example code:
     *
     * @code
     * radio.stopListening();
     * radio.write(&data,sizeof(data));
     * @endcode
     *
     * @return
     * - `true` if the payload was delivered successfully and an acknowledgement
     *   (ACK packet) was received. If auto-ack is disabled, then any attempt
     *   to transmit will also return true (even if the payload was not
     *   received).
     * - `false` if the payload was sent but was not acknowledged with an ACK
     *   packet. This condition can only be reported if the auto-ack feature
     *   is on.
     */
    bool write(const void *buf, unsigned int len)
    {
        return NrfusbWrite(self, buf, len);
    }

    /**
     * New: Open a pipe for writing via byte array. Old addressing format retained for compatibility.
     *
     * Only pipe 0 can be opened as writing, but the destination address can be changed. Be sure to call
     * Nrfusb::stopListening() prior to calling this function. Addresses are assigned via a byte array and the default
     * is 5 byte address length.
     *
     * Example code 1:
     *
     * @code
     * char addresses[][6] = {"1Node", "2Node"};
     * radio.openWritingPipe(addresses[0]);
     * @endcode
     *
     * Example code 2:
     *
     * @code
     * char address[] = { 0xCC, 0xCE, 0xCC, 0xCE, 0xCC };
     * radio.openWritingPipe(address);
     * address[0] = 0x33;
     * radio.openReadingPipe(1, address);
     * @endcode
     *
     * @warning This function will overwrite the address set to reading pipe 0
     * as stipulated by the datasheet for proper auto-ack functionality in TX
     * mode. Use this function to ensure proper transmission acknowledgement
     * when the address set to reading pipe 0 (via Nrfusb::openReadingPipe()) does not
     * match the address passed to this function. If the auto-ack feature is
     * disabled, then this function will still overwrite the address for
     * reading pipe 0 regardless.
     *
     * @see
     * - Nrfusb::setAddressWidth()
     * - Nrfusb::startListening()
     *
     * @param address The address to be used for outgoing transmissions (uses
     * pipe 0). Coordinate this address amongst other receiving nodes (the
     * pipe numbers don't need to match).
     *
     * @remark There is no address length parameter because this function will
     * always write the number of bytes that the radio addresses are configured
     * to use (set with Nrfusb::setAddressWidth()).
     */
    void openWritingPipe(const uint8_t *address)
    {
        NrfusbOpenWritingPipe(self, address);
    }

    /**
     * Open a pipe for reading.
     *
     * Up to 6 pipes can be open for reading at once. Open all the required
     * reading pipes, and then call Nrfusb::startListening().
     *
     * @see
     * - Nrfusb::openWritingPipe()
     * - Nrfusb::setAddressWidth()
     *
     * @note Pipes 0 and 1 will store a full 5-byte address. Pipes 2-5 will technically
     * only store a single byte, borrowing up to 4 additional bytes from pipe 1 per the
     * assigned address width.
     * Pipes 1-5 should share the same address, except the first byte.
     * Only the first byte in the array should be unique, e.g.
     * @code
     * char addresses[][6] = {"Prime", "2Node", "3xxxx", "4xxxx"};
     * openReadingPipe(0, addresses[0]); // address used is "Prime"
     * openReadingPipe(1, addresses[1]); // address used is "2Node"
     * openReadingPipe(2, addresses[2]); // address used is "3Node"
     * openReadingPipe(3, addresses[3]); // address used is "4Node"
     * @endcode
     *
     * @warning
     * @parblock
     * If the reading pipe 0 is opened by this function, the address
     * passed to this function (for pipe 0) will be restored at every call to
     * Nrfusb::startListening().
     *
     * Read
     * http://maniacalbits.blogspot.com/2013/04/rf24-addressing-nrf24l01-radios-require.html
     * to understand how to avoid using malformed addresses. This address
     * restoration is implemented because of the underlying necessary
     * functionality of Nrfusb::openWritingPipe().
     * @endparblock
     *
     * @param number Which pipe to open. Only pipe numbers 0-5 are available,
     * an address assigned to any pipe number not in that range will be ignored.
     * @param address The 3, 4 or 5 bytes of the address to open on the pipe.
     *
     * There is no address length parameter because this function will
     * always write the number of bytes (for pipes 0 and 1) that the radio
     * addresses are configured to use (set with Nrfusb::setAddressWidth()).
     */
    void openReadingPipe(uint8_t pipe_number, const uint8_t* address)
    {
        NrfusbOpenReadingPipe(self, pipe_number, address);
    }

    /**@}*/
    /**
     * @name Advanced Operation
     *
     * Methods you can use to drive the chip in more advanced ways.
     */
    /**@{*/

    /**
     * If a failure has been detected, it usually indicates a hardware issue. By default the library
     * will cease operation when a failure is detected. This should allow advanced users to detect and resolve
     * intermittent hardware issues.
     *
     * @warning The Nrfusb failure handling is a little different from RF24 library. In RF24 the failureDetect is a
     *          property of the RF24 class. Here failureDetect is a method and it can be set with setFailureDetectTo.
     *          Furthermore, the get method will not read from the board because the failure detect is always sent
     *          after a write operation.
     *
     * In most cases, the radio must be re-enabled via
     * @code
     * radio.begin();
     * @endcode
     *
     * and the appropriate settings applied after a failure occurs, if wanting to re-enable the device immediately.
     *
     * The three main failure modes of the radio include:
     *
     * 1. Writing to radio: Radio unresponsive
     *     - Fixed internally by adding a timeout to the internal write functions in RF24 (failure handling)
     * 2. Reading from radio: Available returns true always
     *     - Fixed by adding a timeout to available functions by the user. This is implemented internally in  RF24Network.
     * 3. Radio configuration settings are lost
     *     - Fixed by monitoring a value that is different from the default, and re-configuring the radio if this setting reverts to the default.
     *
     * See the example from RF24 library: GettingStarted_HandlingFailures.
     *
     * @code
     * if(radio.failureDetected()) {
     *   radio.begin();                          // Attempt to re-configure the radio with defaults
     *   radio.setFailureDetectedTo(0);          // Reset the detection value
     *   radio.openWritingPipe(addresses[1]);    // Re-configure pipe addresses
     *   radio.openReadingPipe(1, addresses[0]);
     *   report_failure();                       // Blink LEDs, send a message, etc. to indicate failure
     * }
     * @endcode
     */
    bool failureDetected()
    {
        return NrfusbFailureDetected(self);
    }

//    void printDetails(void);

//    void printPrettyDetails(void);

//    uint16_t sprintfPrettyDetails(char *debugging_information);

//    void encodeRadioDetails(uint8_t *encoded_status);

    /**
     * Test whether there are bytes available to be read from the FIFO buffers.
     *
     * @param[out] pipe_num Which pipe has the payload available
     * @code
     * unsigned int pipeNum;
     * if(radio.available(&pipeNum)){
     *   radio.read(&data, sizeof(data));
     *   std::cout << "Received data on pipe " pipeNum<< std::endl;
     * }
     * @endcode
     *
     * @note Differently from the RF24 documentation, the whatHappened method (unavailable in this driver) is
     *       automatically called by the board and therefore you do not need to worry about this detail.
     *
     * @return
     * - `true` if there is a payload available in the top (first out) level RX FIFO.
     * - `false` if there is nothing available in the RX FIFO because it is empty.
     */
    bool available(unsigned int* pipe_num)
    {
        return NrfusbAvailable(self, pipe_num);
    }

    bool rxFifoFull ()
    {
        return NrfusbRxFifoFull(self);
    }

    uint8_t isFifo (bool about_tx)
    {
        return NrfusbIsFifo(self, about_tx);
    }

    bool isFifo (bool about_tx, bool check_empty)
    {
        return NrfusbIsFifo(self, about_tx, check_empty);
    }

    /**
     * Enter low-power mode.
     *
     * To return to normal power mode, call Nrfusb::powerUp().
     *
     * @note After calling Nrfusb::startListening(), a basic radio will consume about 13.5mA at max PA level.
     * During active transmission, the radio will consume about 11.5mA, but this will be reduced to 26uA (.026mA)
     * between sending. In full powerDown mode, the radio will consume approximately 900nA (.0009mA)
     *
     * Example code:
     *
     * @code
     * radio.powerDown();
     * // do or wait something to happen
     * radio.powerUp();
     * @endcode
     */
    void powerDown()
    {
        NrfusbPowerDown(self);
    }

    /**
     * Leave low-power mode.
     *
     * It is required to call this function for normal radio operation after calling Nrfusb::powerDown().
     *
     * To return to low power mode, call Nrfusb::powerDown().
     *
     * @note This will take up to 5ms for maximum compatibility and the firmware on the Nrfusb will make the board wait
     *       this time.
     */
    void powerUp()
    {
        NrfusbPowerUp(self);
    }

    /**
     * Write for single NOACK writes.
     *
     * Optionally disable acknowledgements/auto-retries for a single payload using the multicast parameter set to true.
     * Can be used with Nrfusb::enableAckPayload() to request a response.
     * @see
     * - Nrfusb::setAutoAck()
     * - Nrfusb::write()
     *
     * @param buf Pointer to the data to be sent
     * @param len Number of bytes to be sent
     * @param multicast Request ACK response (false), or no ACK response (true). Be sure to have called
     *                  Nrfusb::enableDynamicAck() at least once before setting this parameter.
     * @return
     * - `true` if the payload was delivered successfully and an acknowledgement (ACK packet) was received. If auto-ack
     *    is disabled, then any attempt to transmit will also return true (even if the payload was not received).
     * - `false` if the payload was sent but was not acknowledged with an ACK packet. This condition can only be
     *    reported if the auto-ack feature is on.
     */
    bool write(const void *buf, unsigned int len, bool multicast)
    {
        return NrfusbWrite(self, buf, len, multicast);
    }

    /**
     * @warning Initial support to this function was added. It probably does not work.
     *
     * This will not block until the 3 FIFO buffers are filled with data. Once the FIFOs are full, this function will
     * simply wait for a buffer to become available or a transmission failure (returning `true` or `false`
     * respectively).
     *
     * @warning
     * @parblock
     * It is important to never keep the nRF24L01 in TX mode and FIFO full for more than 4ms at a time. If the auto
     * retransmit is enabled, the nRF24L01 is never in TX mode long enough to disobey this rule. Allow the FIFO
     * to clear by issuing Nrfusb::txStandBy() or ensure appropriate time between transmissions.
     *
     * Use Nrfusb::txStandBy() when this function returns `false`.
     *
     * Example (Partial blocking):
     * @code
     * radio.writeFast(&buf, 32);  // Writes 1 payload to the buffers
     * txStandBy();     		   // Returns 0 if failed. 1 if success. Blocks only until MAX_RT timeout or success. Data flushed on fail.
     *
     * radio.writeFast(&buf, 32);  // Writes 1 payload to the buffers
     * txStandBy(1000);		       // Using extended timeouts, returns 1 if success. Retries failed payloads for 1 seconds before returning 0.
     * @endcode
     * @endparblock
     *
     * @see
     * - Nrfusb::setAutoAck()
     * - Nrfusb::txStandBy()
     * - Nrfusb::write()
     * - Nrfusb::writeBlocking()
     *
     * @param buf Pointer to the data to be sent.
     * @param len Number of bytes to be sent.
     * @return
     * - `true` if the payload passed to `buf` was loaded in the TX FIFO.
     * - `false` if the payload passed to `buf` was not loaded in the TX FIFO
     *   because a previous payload already in the TX FIFO failed to
     *   transmit. This condition can only be reported if the auto-ack feature
     *   is on.
     */
    bool writeFast(const void *buf, unsigned int len)
    {
        return NrfusbWriteFast(self, buf, len);
    }

    /**
     * @warning Initial support to this function was added. It probably does not work.
     *
     * Similar to writeFast(const void*, uint8_t) but allows for single NOACK writes. Optionally disable
     * acknowledgements/auto-retries for a single payload using the multicast parameter set to `true`.
     *
     * @warning If the auto-ack feature is enabled, then it is strongly encouraged to call Nrfusb::txStandBy() when
     *          this function returns `false`.
     *
     * @see
     * - Nrfusb::setAutoAck()
     * - Nrfusb::txStandBy()
     * - Nrfusb::write()
     * - Nrfusb::writeBlocking()
     *
     * @param buf Pointer to the data to be sent
     * @param len Number of bytes to be sent
     * @param multicast Request ACK response (false), or no ACK response (true). Be sure to have called
     *                  Nrfusb::enableDynamicAck() at least once before setting this parameter.
     * @return
     * - `true` if the payload passed to `buf` was loaded in the TX FIFO.
     * - `false` if the payload passed to `buf` was not loaded in the TX FIFO because a previous payload already in the
     *           TX FIFO failed to transmit. This condition can only be reported if the auto-ack feature is on
     *           (and the multicast parameter is set to false).
     */
    bool writeFast(const void *buf, unsigned int len, bool multicast)
    {
        return NrfusbWriteFast(self, buf, len, multicast);
    }

    /**
     * @warning Initial support to this function was added. It probably does not work.
     *
     * This function extends the auto-retry mechanism to any specified duration. It will not block until the 3 FIFO
     * buffers are filled with data. If so, the library will auto retry until a new payload is written or the user
     * specified timeout period is reached.
     *
     * @warning It is important to never keep the nRF24L01 in TX mode and FIFO full for more than 4ms at a time. If the
     *          auto retransmit is enabled, the nRF24L01 is never in TX mode long enough to disobey this rule. Allow
     *          the FIFO to clear by issuing Nrfusb::txStandBy() or ensure appropriate time between transmissions.
     *
     * Example (Full blocking):
     * @code
     * radio.writeBlocking(&buf, sizeof(buf), 1000); // Wait up to 1 second to write 1 payload to the buffers
     * radio.txStandBy(1000);                        // Wait up to 1 second for the payload to send. Return 1 if ok, 0 if failed.
     *                                               // Blocks only until user timeout or success. Data flushed on fail.
     * @endcode
     *
     * @see
     * - Nrfusb::txStandBy()
     * - Nrfusb::write()
     * - Nrfusb::writeFast()
     *
     * @param buf Pointer to the data to be sent
     * @param len Number of bytes to be sent
     * @param timeout User defined timeout in milliseconds.
     *
     * @return
     * - `true` if the payload passed to `buf` was loaded in the TX FIFO.
     * - `false` if the payload passed to `buf` was not loaded in the TX FIFO because a previous payload already in
     *           the TX FIFO failed to transmit. This condition can only be reported if the auto-ack feature is on.
     */
    bool writeBlocking(const void *buf, unsigned int len, uint32_t timeout)
    {
        return NrfusbWriteBlocking(self, buf, len, timeout);
    }

    bool txStandBy()
    {
        return NrfusbTxStandBy(self);
    }

    bool txStandBy(uint32_t timeout, bool startTx=false)
    {
        return NrfusbTxStandBy(self, timeout, startTx);
    }

    bool writeAckPayload(unsigned int pipe, const void *buf, unsigned int len)
    {
        return NrfusbWriteAckPayload(self, pipe, buf, len);
    }

    void whatHappened(bool &tx_ok, bool &tx_fail, bool &rx_ready)
    {
        NrfusbWhatHappened(self, tx_ok, tx_fail, rx_ready);
    }

    void startFastWrite(const void *buf, uint8_t len, bool multicast, bool startTx=true)
    {
        NrfusbStartFastWrite(self, buf, len, multicast, startTx);
    }

    bool startWrite(const void *buf, uint8_t len, bool multicast)
    {
        return NrfusbStartWrite(self, buf, len, multicast);
    }

    void reUseTX()
    {
        NrfusbReUseTX(self);
    }

    unsigned int flush_tx()
    {
        return NrfusbFlush_tx(self);
    }

    unsigned int flush_rx()
    {
        return NrfusbFlush_rx(self);
    }

    /**
     * Test whether there was a carrier on the line for the previous listening period. Useful to check for interference
     * on the current channel.
     *
     * @return true if was carrier, false if not
     */
    bool testCarrier()
    {
        return NrfusbTestCarrier(self);
    }

    /**
     * Test whether a signal (carrier or otherwise) greater than or equal to -64dBm is present on the channel.
     * Valid only on nRF24L01P (+) hardware. On nRF24L01, use testCarrier(). Useful to check for interference on the
     * current channel and channel hopping strategies.
     *
     * @code
     * bool goodSignal = radio.testRPD();
     * if(radio.available()){
     *    if (goodSignal)
     *        std::cout << "Strong signal > 64dBm" << std::endl;
     *    else
     *        std::cout << "Weak signal < 64dBm" << std::endl;
     *    radio.read(0,0);
     * }
     * @endcode
     * @return true if a signal less than or equal to -64dBm was detected,
     * false if not.
     */
    bool testRPD()
    {
        return NrfusbTestRPD(self);
    }

    /**
     * Test whether this is a real radio, or a mock shim for debugging. Setting either pin to 0xff is the way to
     * indicate that this is not a real radio.
     *
     * @return true if this is a legitimate radio
     */
    bool isValid()
    {
        return NrfusbIsValid(self);
    }

    /**
     * Close a pipe after it has been previously opened. Can be safely called without having previously opened a pipe.
     *
     * @param pipe Which pipe number to close, any integer not in range [0, 5] is ignored.
     */
    void closeReadingPipe(unsigned int pipe_number)
    {
        NrfusbCloseReadingPipe(self, pipe_number);
    }

    /**@}*/
    /**
     * @name Optional Configurators
     *
     * Methods you can use to get or set the configuration of the chip. None are required. Calling Nrfusb::begin() sets
     * up a reasonable set of defaults.
     */
    /**@{*/

    /**
     * The driver will delay for this duration when Nrfusb::stopListening() is called. This wait time can be set using
     * this method.
     *
     * @param txDelay is a 32 bit unsigned integer value.
     *
     * @warning If set to 0, ensure 130uS delay after Nrfusb::stopListening() and before any sends.
     */
    void setTxDelay(unsigned int txDelay)
    {
        NrfusbSetTxDelay(self, txDelay);
    }

    /**
     * The driver will delay for this duration when Nrfusb::stopListening() is called. You can use this method to get
     * how much time the firmware is going to wait.
     *
     * @return a 32 bit unsigned integer value.
     */
    unsigned int getTxDelay()
    {
        return NrfusbGetTxDelay(self);
    }

    /**
     * On all devices but Linux and ATTiny, a small delay is added to the CSN toggling function. This is intended to
     * minimise the speed of SPI polling due to radio commands. Because the firmware is using interrupts, this can be
     * set to 0. The default value is 5.
     *
     * @param csDelay is a 32 bit unsigned integer value.
     */
    void setCsDelay(unsigned int csDelay)
    {
        NrfusbSetCsDelay(self, csDelay);
    }

    /**
     * On all devices but Linux and ATTiny, a small delay is added to the CSN toggling function. This is intended to
     * minimise the speed of SPI polling due to radio commands. You can use this method to get how much time the
     * firmware is going to wait.
     *
     * @return a 32 bit unsigned integer value.
     */
    unsigned int getCsDelay()
    {
        return NrfusbGetCsDelay(self);
    }

    /**
     * Set the address width from 3 to 5 bytes.
     *
     * @param a_width The address width (in bytes) to use; this can be 3, 4 or 5.
     */
    void setAddressWidth(unsigned int a_width)
    {
        NrfusbSetAddressWidth(self, a_width);
    }

    /**
     * Set the number of retry attempts and delay between retry attempts when transmitting a payload. The radio is
     * waiting for an acknowledgement (ACK) packet during the delay between retry attempts.
     *
     * @param delay is how long to wait between each retry, in multiples of 250 us. The minimum of 0 means 250 us, an
     *              the maximum of 15 means 4000 us. The default value of 5 means 1500us (5 * 250 + 250).
     * @param count How many retries before giving up. The default/maximum is 15. Use 0 to disable the auto-retry
     *              feature all together.
     *
     * @note Disable the auto-retry feature on a transmitter still uses the auto-ack feature (if enabled), except it
     *       will not retry to transmit if the payload was not acknowledged on the first attempt.
     */
    void setRetries(unsigned int delay, unsigned int count)
    {
        NrfusbSetRetries(self, delay, count);
    }

    /**
     * Set RF communication channel.
     *
     * The frequency used by a channel is calculated as:
     * @verbatim 2400 MHz + <channel number> @endverbatim
     *
     * The default channel of 76 uses the approximate frequency of 2476 MHz.
     *
     * @param channel Which RF channel to communicate on, 0-125.
     */
    void setChannel(unsigned int new_channel)
    {
        NrfusbSetChannel(self, new_channel);
    }

    /**
     * Get RF communication channel.
     *
     * @return The currently configured RF Channel
     */
    int getChannel()
    {
        return NrfusbGetChannel(self);
    }

    /**
     * Set static payload size.
     *
     * This implementation uses a pre-established fixed payload size for all transmissions. If this method is never
     * called, the driver will always transmit the maximum payload size (32 bytes), no matter how much was sent to
     * Nrfusb::write().
     *
     * @param size is the number of bytes in the payload.
     */
    void setPayloadSize(unsigned int new_payload_size)
    {
        NrfusbSetPayloadSize(self, new_payload_size);
    }

    /**
     * Get static payload size.
     *
     * @see Nrfusb::setPayloadSize()
     *
     * @return The number of bytes in the payload.
     */
    unsigned int getPayloadSize()
    {
        return NrfusbGetPayloadSize(self);
    }

    /**
     * Get dynamic payload size.
     *
     * For dynamic payloads, this pulls the size of the payload off the chip
     *
     * @note Corrupt packets are now detected and flushed per the
     * manufacturer.
     *
     * Example code:
     *
     * @code
     * if(radio.available()){
     *   if(radio.getDynamicPayloadSize() < 1) {
     *     // Corrupt payload has been flushed
     *     return;
     *   }
     *   radio.read(&data, sizeof(data));
     * }
     * @endcode
     *
     * @return Payload length of last-received dynamic payload.
     */
    unsigned int getDynamicPayloadSize()
    {
        return NrfusbGetDynamicPayloadSize(self);
    }

    /**
     * Enable custom payloads in the acknowledge packets.
     *
     * ACK payloads are a handy way to return data back to senders without manually changing the radio modes on both
     * units.
     *
     * @remarks The ACK payload feature requires the auto-ack feature to be enabled for any pipe using ACK payloads.
     *          This function does not automatically enable the auto-ack feature on pipe 0 since the auto-ack feature
     *          is enabled for all pipes by default.
     *
     * @see Nrfusb::setAutoAck()
     *
     * @note ACK payloads are dynamic payloads. This function automatically enables dynamic payloads on pipes 0 & 1 by
     *       default. Call enableDynamicPayloads() to enable on all pipes (especially for RX nodes that use pipes other
     *       than pipe 0 to receive transmissions expecting responses with ACK payloads).
     */
    void enableAckPayload()
    {
        NrfusbEnableAckPayload(self);
    }

    /**
     * Disable custom payloads on the acknowledge packets.
     *
     * @see Nrfusb::enableAckPayload()
     */
    void disableAckPayload()
    {
        NrfusbDisableAckPayload(self);
    }

    /**
     * Enable dynamically-sized payloads.
     *
     * This way you don't always have to send large packets just to send them once in a while. This enables
     * dynamic payloads on ALL pipes.
     *
     */
    void enableDynamicPayloads()
    {
        NrfusbEnableDynamicPayloads(self);
    }

    /**
     * Disable dynamically-sized payloads.
     *
     * This disables dynamic payloads on ALL pipes. Since Ack Payloads requires Dynamic Payloads, Ack Payloads
     * are also disabled. If dynamic payloads are later re-enabled and ack payloads are desired
     * Nrfusb::enableAckPayload() must be called again as well.
     *
     */
    void disableDynamicPayloads()
    {
        NrfusbDisableDynamicPayloads(self);
    }

    /**
     * Enable dynamic ACKs (single write multicast or unicast) for chosen messages.
     *
     * @note This function must be called once before using the multicast parameter for any functions that offer it.
     *       To use multicast behavior about all outgoing payloads (using pipe 0) or incoming payloads
     *       (concerning all RX pipes), use Nrfusb::setAutoAck().
     *
     * @see
     * - Nrfusb::setAutoAck() for all pipes
     * - Nrfusb::setAutoAck(unsigned int, bool) for individual pipes
     *
     * @code
     * radio.write(&data, 32, 1); // Sends a payload with no acknowledgement requested
     * radio.write(&data, 32, 0); // Sends a payload using auto-retry/autoACK
     * @endcode
     */
    void enableDynamicAck()
    {
        NrfusbEnableDynamicAck(self);
    }

    /**
     * Determine whether the hardware is an nRF24L01+ or not.
     *
     * @return true if the hardware is nRF24L01+ (or compatible) and false if its not.
     */
    bool isPVariant()
    {
        return NrfusbIsPVariant(self);
    }

    /**
     * Enable or disable the auto-acknowledgement feature for all pipes.
     *
     * This feature is enabled by default. Auto-acknowledgement responds to every received payload with an empty
     * ACK packet. These ACK packets get sent from the receiving radio back to the transmitting radio. To attach an
     * ACK payload to a ACK packet, use Nrfusb::writeAckPayload().
     *
     * If this feature is disabled on a transmitting radio, then the transmitting radio will always report that the
     * payload was received (even if it was not). Please remember that this feature's configuration needs to match
     * for transmitting and receiving radios.
     *
     * @warning When using the `multicast` parameter to Nrfusb::write(), this feature can be disabled for an individual
     *          payload. However, if this feature is disabled, then the `multicast` parameter will have no effect.
     *
     * @note If disabling auto-acknowledgment packets, the ACK payloads feature is also disabled as this feature is
     *       required to send ACK payloads.
     *
     * @see
     * - Nrfusb::write()
     * - Nrfusb::writeFast()
     * - Nrfusb::startFastWrite()
     * - Nrfusb::startWrite()
     * - Nrfusb::writeAckPayload()
     *
     * @param enable Whether to enable (true) or disable (false) the auto-acknowledgment feature for all pipes.
     */
    void setAutoAck(bool enable)
    {
        NrfusbSetAutoAck(self, enable);
    }

    /**
     * Enable or disable the auto-acknowledgement feature for a specific pipe.
     *
     * This feature is enabled by default for all pipes. Auto-acknowledgement responds to every received payload with
     * an empty ACK packet. These ACK packets get sent from the receiving radio back to the transmitting radio.
     * To attach an ACK payload to a ACK packet, use Nrfusb::writeAckPayload().
     *
     * Pipe 0 is used for TX operations, which include sending ACK packets. If using this feature on both TX & RX
     * nodes, then pipe 0 must have this feature enabled for the RX & TX operations. If this feature is disabled
     * on a transmitting radio's pipe 0, then the transmitting radio will always report that the payload was received
     * (even if it was not). Remember to also enable this feature for any pipe that is openly listening to a
     * transmitting radio with this feature enabled.
     *
     * @warning If this feature is enabled for pipe 0, then the `multicast` parameter to Nrfusb::write() can be used
     *          to disable this feature for an individual payload. However, if this feature is disabled for pipe 0,
     *          then the `multicast` parameter will have no effect.
     *
     * @note If disabling auto-acknowledgment packets on pipe 0, the ACK payloads feature is also disabled as this
     *       feature is required on pipe 0 to send ACK payloads.
     *
     * @see
     * - write()
     * - writeFast()
     * - startFastWrite()
     * - startWrite()
     * - writeAckPayload()
     * - enableAckPayload()
     * - disableAckPayload()
     *
     * @param pipe Which pipe to configure. This number should be in range [0, 5].
     * @param enable Whether to enable (true) or disable (false) the auto-acknowledgment feature for the specified pipe.
     */
    void setAutoAck(unsigned int pipe_number, bool enable)
    {
        NrfusbSetAutoAck(self, pipe_number, enable);
    }

    /**
     * Set Power Amplifier (PA) level and Low Noise Amplifier (LNA) state.
     *
     * @param level The desired @ref PALevel as defined by @ref rf24_pa_dbm_e.
     * @param lnaEnable Enable or Disable the LNA (Low Noise Amplifier) Gain.
     *
     * See table for Si24R1 modules below. @p lnaEnable only affects nRF24L01 modules with an LNA chip.
     *
     * | @p level (enum value) | nRF24L01<br>description | Si24R1<br>description when<br> @p lnaEnable = 1 | Si24R1<br>description when<br> @p lnaEnable = 0 |
     * |:---------------------:|:-------:|:--------:|:-------:|
     * | @ref RF24_PA_MIN (0)  | -18 dBm |  -6 dBm  | -12 dBm |
     * | @ref RF24_PA_LOW (1)  | -12 dBm |  -0 dBm  | -4 dBm  |
     * | @ref RF24_PA_HIGH (2) | -6 dBm  |  3 dBm   | 1 dBm   |
     * | @ref RF24_PA_MAX (3)  |  0 dBm  |  7 dBm   | 4 dBm   |
     *
     * @note The Nrfusb::getPALevel() function does not care what was passed @p lnaEnable parameter.
     */
    void setPALevel(rf24_pa_dbm_e level, bool lnaEnable=true)
    {
        NrfusbSetPALevel(self, level, lnaEnable);
    }

    /**
     * Fetches the current @ref PALevel.
     *
     * @return One of the values defined by @ref rf24_pa_dbm_e.
     * See tables in @ref rf24_pa_dbm_e or Nrfusb::setPALevel().
     */
    rf24_pa_dbm_e getPALevel()
    {
        return NrfusbGetPALevel(self);
    }

    /**
     * Returns automatic retransmission count (ARC_CNT).
     *
     * Value resets with each new transmission. Allows roughly estimating signal strength.
     *
     * @return Returns values from 0 to 15.
     */
    unsigned int getARC()
    {
        return NrfusbGetARC(self);
    }

    /**
     * Set the transmission @ref Datarate.
     *
     * @warning setting @ref RF24_250KBPS will fail for non-plus modules (when Nrfusb::isPVariant() returns false).
     *
     * @param speed Specify one of the following values (as defined by @ref rf24_datarate_e):
     * | @p speed (enum value) | description  |
     * |:---------------------:|:------------:|
     * | @ref RF24_1MBPS (0)   | for 1 Mbps   |
     * | @ref RF24_2MBPS (1)   | for 2 Mbps   |
     * | @ref RF24_250KBPS (2) | for 250 kbps |
     *
     * @return true if the change was successful (not implemented yet).
     */
    bool setDataRate(rf24_datarate_e speed)
    {
        return NrfusbSetDataRate(self, speed);
    }

    /**
     * Fetches the currently configured transmission @ref Datarate.
     *
     * @return One of the values defined by @ref rf24_datarate_e.
     *
     * See table in @ref rf24_datarate_e or Nrfusb::setDataRate()
     */
    rf24_datarate_e getDataRate()
    {
        return NrfusbGetDataRate(self);
    }

    /**
     * Set the @ref CRCLength (in bits).
     *
     * It is worth noting that the CRC can not be disabled if auto-ack is enabled.
     *
     * @param length Specify one of the values (as defined by @ref rf24_crclength_e)
     * | @p length (enum value)     | description                    |
     * |:--------------------------:|:------------------------------:|
     * | @ref RF24_CRC_DISABLED (0) | to disable using CRC checksums |
     * | @ref RF24_CRC_8 (1)        | to use 8-bit checksums         |
     * | @ref RF24_CRC_16 (2)       | to use 16-bit checksums        |
     */
    void setCRCLength(rf24_crclength_e length)
    {
        NrfusbSetCRCLength(self, length);
    }

    /**
     * Get the @ref CRCLength (in bits).
     *
     * It is worth noting that CRC checking cannot be disabled if auto-ack is enabled.
     *
     * @return One of the values defined by @ref rf24_crclength_e.
     *
     * See table in @ref rf24_crclength_e or Nrfusb::setCRCLength().
     */
    rf24_crclength_e getCRCLength()
    {
        return NrfusbGetCRCLength(self);
    }

    /**
     * Disable CRC validation.
     *
     * @warning CRC cannot be disabled if auto-ack/ESB is enabled.
     */
    void disableCRC()
    {
        NrfusbDisableCRC(self);
    }

    void maskIRQ(bool tx_ok, bool tx_fail, bool rx_ready)
    {
        NrfusbMaskIRQ(self, tx_ok, tx_fail, rx_ready);
    }

    void startConstCarrier(rf24_pa_dbm_e level, unsigned int channel)
    {
        NrfusbStartConstCarrier(self, level, channel);
    }

    void stopConstCarrier()
    {
        NrfusbStopConstCarrier(self);
    }

    void toggleAllPipes(bool isEnabled)
    {
        NrfusbToggleAllPipes(self, isEnabled);
    }

    /**
     * @brief configure the RF_SETUP register in 1 transaction.
     *
     * @param level This parameter is the same input as Nrfusb::setPALevel()'s `level` parameter.
     *              See @ref rf24_pa_dbm_e enum for accepted values.
     * @param speed This parameter is the same input as Nrfusb::setDataRate()'s `speed` parameter.
     *              See @ref rf24_datarate_e enum for accepted values.
     * @param lnaEnable This optional parameter is the same as Nrfusb::setPALevel()'s `lnaEnable` optional parameter.
     *                  Defaults to `true` (meaning LNA feature is enabled) when not specified.
     */
    void setRadiation(rf24_pa_dbm_e level, rf24_datarate_e speed, bool lnaEnable=true)
    {
        NrfusbSetRadiation(self, level, speed, lnaEnable);
    }

    /**
     * Enables the RX led.
     */
    void ledRxOn()
    {
        NrfusbLedRxOn(self);
    }

    /**
     * Disables the RX led.
     */
    void ledRxOff()
    {
        NrfusbLedRxOff(self);
    }

    /**
     * Enables the TX led.
     */
    void ledTxOn()
    {
        NrfusbLedTxOn(self);
    }

    /**
     * Disables the TX led.
     */
    void ledTxOff()
    {
        NrfusbLedTxOff(self);
    }

    bool comboWrite(const void *buf, unsigned int len, bool multicast=0)
    {
        return NrfusbComboWrite(self, buf, len, multicast);
    }

    /**@}*/

private:
    std::shared_ptr<NrfusbImpl> self;
};

#endif //NRFUSB_HPP
