#include "nrfusb.hpp"
#include <string>

Nrfusb *radio;
uint64_t ti, tf;

int main()
{
#if WIN32
    std::string portName = "COM4";
#else
    std::string portName = "/dev/ttyACM0";
#endif

    radio = new Nrfusb(portName);
    std::cout << "Channel before: " << radio->getChannel() << std::endl;
    radio->setChannel(113);
    std::cout << "Channel now:    " << radio->getChannel() << std::endl;

    return 0;
}
