#include "nrfusb.hpp"
#include <memory.h>

Nrfusb *radio;
uint64_t ti, tf;

void myCallback(bool tx_ok, bool tx_fail, bool rx_ready)
{
    (void) tx_ok;
    (void) tx_fail;
    (void) rx_ready;

    uint8_t data[32];

    if (rx_ready)
    {
        if (radio->available())
        {
            radio->read(data, 32);
            if (data[0] == 'b')
            {
                data[0] = 0;
            }

            printf("Elapsed time = %d us\n", int(getTimeUs() - ti));
        }
    }
}

int main()
{
#if WIN32
    radio = new Nrfusb((char*) "COM5");
#else
    radio = new Nrfusb("/dev/ttyACM0", myCallback);
#endif

    const uint8_t pcAddress[] = {'T', 'X', 'a', 'a', 'a'};
    const uint8_t boardAddress[] = {'R', 'X', 'a', 'a', 'a'};

    uint8_t ledOn[]  = {255, 255, 3, 4, 127, 1, 0, 120};
    uint8_t ledOff[] = {255, 255, 3, 4, 127, 1, 4, 116};

    radio->maskIRQ(true, true, false);
    radio->setChannel(113);
    radio->setDataRate(RF24_2MBPS);
    radio->setCRCLength(RF24_CRC_8);
    radio->openWritingPipe(boardAddress);
    radio->openReadingPipe(1, pcAddress);
    radio->setRetries(3, 5);

    uint8_t txData[32];
    memset(txData, 0, 32);
    txData[0] = 'a';

    while ( true )
    {
        ti = getTimeUs();
        radio->stopListening();
        radio->write(txData, 32);
        radio->startListening();

        delayMs(100 - (getTimeUs() - ti) / 1000);


//        // Send the instruction and collect the automatic answer
//        ti = getTimeUs();
//        result = radio->write(ledOff, 8);
//        if (result)
//        {
//            if (radio->available())
//            {
//                size = radio->getDynamicPayloadSize();
//                radio->read(buffer, size);
//                tf = getTimeUs();
//                printf("Elapsed time = %d us\n", int(tf - ti));
//            }
//        }
//
//        delayMs(100);
//
//        // Send the instruction and collect the automatic answer
//        ti = getTimeUs();
//        result = radio->write(ledOn, 8);
//        if (result)
//        {
//            if (radio->available())
//            {
//                size = radio->getDynamicPayloadSize();
//                radio->read(buffer, size);
//                tf = getTimeUs();
//                printf("Elapsed time = %d us\n", int(tf - ti));
//            }
//        }
//
//        delayMs(100);
    }

    return 0;
}
