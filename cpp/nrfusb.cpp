#ifdef WIN32
/*
 * Asio library requires some Windows definitions available at this file.
 */
#include <SDKDDKVer.h>

/*
 * The following library adds support to __nop instruction.
 * https://stackoverflow.com/questions/54918884/implementations-for-asmnop-in-windows
 */
#include <intrin.h>
#endif

/*
 * Include the asio library and build it as static library. More information can be found at:
 * https://think-async.com/Asio/asio-1.18.0/doc/asio/using.html
 */
#define ASIO_SEPARATE_COMPILATION
#include <asio.hpp>
#include <asio/impl/src.hpp>

#include <algorithm>
#include <functional>
#include <memory>
#include <vector>
#include <mutex>
#include <thread>
#include <utility>
#include <string>

#include <chrono>
using namespace std::chrono;

#include <system_error>
#include "receiver.h"
#include "nrfusb_definitions.hpp"

#ifdef WIN32
#include <windows.h>
#include <winternl.h>
extern "C" NTSYSAPI NTSTATUS NTAPI NtSetTimerResolution(ULONG DesiredResolution, BOOLEAN SetResolution, PULONG CurrentResolution);
#endif

#define u8(x) static_cast<uint8_t>(x)

constexpr uint8_t BUFFER_SIZE = 40;

uint64_t getTimeMs()
{
    const uint64_t now = duration_cast<milliseconds>(time_point_cast<milliseconds>(high_resolution_clock::now()).time_since_epoch()).count();
    return now;
}

uint64_t getTimeUs()
{
    const uint64_t now = duration_cast<microseconds>(time_point_cast<microseconds>(high_resolution_clock::now()).time_since_epoch()).count();
    return now;
}

void delayMs(uint64_t time_to_wait)
{
    const uint64_t deadline = 1000UL * time_to_wait + getTimeUs();
    while (deadline > getTimeUs())
    {
#if WIN32
        __nop();
#else
        asm("nop");
#endif
    }
}

void delayUs(uint64_t time_to_wait)
{
    const uint64_t deadline = time_to_wait + getTimeUs();
    while (deadline > getTimeUs())
    {
#if WIN32
        __nop();
#else
        asm("nop");
#endif
    }
}

struct NrfusbImpl {
    asio::io_context* asioContext;
    asio::serial_port* serialPort;
    std::mutex* mutex;
    std::thread threadAsio;
    uint8_t* rxBuffer;
    bool waitingRx;
    bool waitingTx;
    message_t rxMsg, txMsg;
    receiver_t usbReceiver;
    bool running;
    unsigned int timeoutMs;
    unsigned int maxRetries;
    std::function<void(bool, bool, bool)> userCallback;

#ifdef WIN32
    ULONG oldResolution;
#endif
};

void prvRead(const std::shared_ptr<NrfusbImpl>& self);
void prvWrite(const std::shared_ptr<NrfusbImpl>& self, uint8_t* buffer, uint8_t size);

void prvCommunicate(const std::shared_ptr<NrfusbImpl>& self)
{
    uint64_t deadlineUs;
    int retries = self->maxRetries;
    uint8_t txSize = messageGetTotalSize(&self->txMsg);
    self->waitingRx = true;
    while ((retries > 0) && self->waitingRx)
    {
        // Send the instruction to the nrfusb board
        prvWrite(self, self->txMsg.data, txSize);

        // Now we wait for the board response
        deadlineUs = getTimeUs() + (self->timeoutMs * 1000);
        while ((deadlineUs > getTimeUs()) && self->waitingRx)
        {
#if WIN32
            __nop();
#else
            asm("nop");
#endif
        }

        // One try is gone
        retries = retries - 1;
    }
}

void prvThreadRun(const std::shared_ptr<NrfusbImpl>& self)
{
    while ( self->running )
    {
        // it may happen that the context is empty, and it requires to be restarted
        // so let's schedule an async_read to make sure it does not stop again
        prvRead(self);

        self->asioContext->run();
        self->asioContext->restart();
    }
}

void prvCallbackFinishedReceiving(const std::shared_ptr<NrfusbImpl>& self, const std::error_code& ec, size_t bytesTransferred)
{
    // prevent warnings about unused variable ec
    (void) ec;
    size_t i;
    bool tx_ok, tx_fail, rx_ready;

    for (i = 0; i < bytesTransferred; i++)
    {
        receiverProcess(&self->usbReceiver, self->rxBuffer[i]);
        if (receiverHasNewMessage(&self->usbReceiver))
        {
            // Verify if received the expected answer (the answers are cmds +1)
            if (self->rxMsg.data[0] == (self->txMsg.data[0] + 1))
            {
                self->waitingRx = false;
            }
            else if (self->rxMsg.data[0] == ansRadioIrq)
            {
                tx_ok = static_cast<bool>(self->rxMsg.data[2]);
                tx_fail = static_cast<bool>(self->rxMsg.data[3]);
                rx_ready = static_cast<bool>(self->rxMsg.data[4]);
                if (self->userCallback != nullptr)
                {
                    std::thread userThread(self->userCallback, tx_ok, tx_fail, rx_ready);
                    userThread.detach();
                }
            }

            receiverReset(&self->usbReceiver);
        }
    }

    // Request an async read operation
    prvRead(self);
}

void prvCallbackFinishedSending(const std::shared_ptr<NrfusbImpl>& self, const std::error_code& ec, size_t bytesTransferred)
{
    (void) ec;
    (void) bytesTransferred;

    // because all bytes were transferred, we release the waiting
    if (self->waitingTx) self->waitingTx = false;
}

void prvRead(const std::shared_ptr<NrfusbImpl>& self)
{
    asio::async_read(
            *self->serialPort,
            asio::buffer(self->rxBuffer, BUFFER_SIZE),
            asio::transfer_at_least(1),
            std::bind(
                    prvCallbackFinishedReceiving,
                    self,
                    std::placeholders::_1,  // asio::placeholders::error_code
                    std::placeholders::_2   // asio::placeholders::bytes_transferred
            )
    );
}

void prvWrite(const std::shared_ptr<NrfusbImpl>& self, uint8_t* buffer, uint8_t size)
{
    asio::async_write(
            *self->serialPort,
            asio::buffer(buffer, size),
            asio::transfer_exactly(size),
            std::bind(
                    prvCallbackFinishedSending,
                    self,
                    std::placeholders::_1,  // asio::placeholders::error_code
                    std::placeholders::_2   // asio::placeholders::bytes_transferred
            )
    );
}

bool NrfusbIsChipConnected(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdIsChipConnected);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbStartListening(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdStartListening);
    prvCommunicate(self);
}

void NrfusbStopListening(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdStopListening);
    prvCommunicate(self);
}

bool NrfusbAvailable(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdAvailable1);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbRead(const std::shared_ptr<NrfusbImpl>& self, void *buf, unsigned int len)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdRead);
    messageAppendParameter(&self->txMsg, len);
    prvCommunicate(self);
    memcpy(buf, &self->rxMsg.data[2], len);
}

bool NrfusbWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdWrite1);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbOpenWritingPipe(const std::shared_ptr<NrfusbImpl>& self, const uint8_t *address)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdOpenWritingPipe);
    messageAppendParameters(&self->txMsg, address, 5);
    prvCommunicate(self);
}

void NrfusbOpenReadingPipe(const std::shared_ptr<NrfusbImpl>& self, uint8_t pipe_number, const uint8_t* address)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

   messageCreate(&self->txMsg, cmdOpenReadingPipe);
   messageAppendParameter(&self->txMsg, pipe_number);
   messageAppendParameters(&self->txMsg, address, 5);
   prvCommunicate(self);
}

bool NrfusbFailureDetected(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetFailureDetected);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbAvailable(const std::shared_ptr<NrfusbImpl>& self, unsigned int* pipe_num)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdAvailable2);
    prvCommunicate(self);
    *pipe_num = self->rxMsg.data[3];
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbRxFifoFull(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdRxFifoFull);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

uint8_t NrfusbIsFifo(const std::shared_ptr<NrfusbImpl>& self, bool about_tx)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdIsFifo1);
    messageAppendParameter(&self->txMsg, u8(about_tx));
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbIsFifo(const std::shared_ptr<NrfusbImpl>& self, bool about_tx, bool check_empty)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdIsFifo2);
    messageAppendParameter(&self->txMsg, u8(about_tx));
    messageAppendParameter(&self->txMsg, u8(check_empty));
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbPowerDown(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdPowerDown);
    prvCommunicate(self);
}

void NrfusbPowerUp(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdPowerUp);
    prvCommunicate(self);
}

bool NrfusbWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, bool multicast)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdWrite2);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameter(&self->txMsg, u8(multicast));
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbWriteFast(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdWriteFast1);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbWriteFast(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, bool multicast)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdWriteFast2);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameter(&self->txMsg, u8(multicast));
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbWriteBlocking(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, uint32_t timeout)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdWriteBlocking);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameters(&self->txMsg, &timeout, 4);
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbTxStandBy(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdTxStandBy1);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbTxStandBy(const std::shared_ptr<NrfusbImpl>& self, uint32_t timeout, bool startTx)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdTxStandBy2);
    messageAppendParameters(&self->txMsg, &timeout, 4);
    messageAppendParameter(&self->txMsg, u8(startTx));
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbWriteAckPayload(const std::shared_ptr<NrfusbImpl>& self, unsigned int pipe, const void *buf, unsigned int len)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdWriteAckPayload);
    messageAppendParameter(&self->txMsg, pipe);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbWhatHappened(const std::shared_ptr<NrfusbImpl>& self, bool &tx_ok, bool &tx_fail, bool &rx_ready)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdWhatHappened);
    prvCommunicate(self);
    tx_ok = static_cast<bool>(self->rxMsg.data[2]);
    tx_fail = static_cast<bool>(self->rxMsg.data[3]);
    rx_ready = static_cast<bool>(self->rxMsg.data[4]);
}

void NrfusbStartFastWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, uint8_t len, bool multicast, bool startTx)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdStartFastWrite);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameter(&self->txMsg, u8(multicast));
    messageAppendParameter(&self->txMsg, u8(startTx));
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
}

bool NrfusbStartWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, uint8_t len, bool multicast)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdStartWrite);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameter(&self->txMsg, u8(multicast));
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbReUseTX(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdReUseTX);
    prvCommunicate(self);
}

unsigned int NrfusbFlush_tx(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdFlush_tx);
    prvCommunicate(self);
    return self->rxMsg.data[2];
}

unsigned int NrfusbFlush_rx(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdFlush_rx);
    prvCommunicate(self);
    return self->rxMsg.data[2];
}

bool NrfusbTestCarrier(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdTestCarrier);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbTestRPD(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdTestRPD);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

bool NrfusbIsValid(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdIsValid);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbCloseReadingPipe(const std::shared_ptr<NrfusbImpl>& self, unsigned int pipe_number)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdCloseReadingPipe);
    messageAppendParameter(&self->txMsg, pipe_number);
    prvCommunicate(self);
}

void NrfusbSetTxDelay(const std::shared_ptr<NrfusbImpl>& self, unsigned int txDelay)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    const uint32_t aux = txDelay;
    messageCreate(&self->txMsg, cmdSetTxDelay);
    messageAppendParameters(&self->txMsg, &aux, 4);
    prvCommunicate(self);
}

unsigned int NrfusbGetTxDelay(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    uint32_t txDelay;
    messageCreate(&self->txMsg, cmdGetTxDelay);
    prvCommunicate(self);
    memcpy(&txDelay, &self->rxMsg.data[2], 4);
    return txDelay;
}

void NrfusbSetCsDelay(const std::shared_ptr<NrfusbImpl>& self, unsigned int csDelay)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    const uint32_t aux = csDelay;
    messageCreate(&self->txMsg, cmdSetCsDelay);
    messageAppendParameters(&self->txMsg, &aux, 4);
    prvCommunicate(self);
}

unsigned int NrfusbGetCsDelay(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    uint32_t csDelay;
    messageCreate(&self->txMsg, cmdGetCsDelay);
    prvCommunicate(self);
    memcpy(&csDelay, &self->rxMsg.data[2], 4);
    return csDelay;
}

void NrfusbSetAddressWidth(const std::shared_ptr<NrfusbImpl>& self, unsigned int a_width)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetAddressWidth);
    messageAppendParameter(&self->txMsg, a_width);
    prvCommunicate(self);
}

void NrfusbSetRetries(const std::shared_ptr<NrfusbImpl>& self, unsigned int delay, unsigned int count)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetRetries);
    messageAppendParameter(&self->txMsg, delay);
    messageAppendParameter(&self->txMsg, count);
    prvCommunicate(self);
}

void NrfusbSetChannel(const std::shared_ptr<NrfusbImpl>& self, unsigned int new_channel)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetChannel);
    messageAppendParameter(&self->txMsg, new_channel);
    prvCommunicate(self);
}

int NrfusbGetChannel(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetChannel);
    prvCommunicate(self);
    return self->rxMsg.data[2];
}

void NrfusbSetPayloadSize(const std::shared_ptr<NrfusbImpl>& self, unsigned int new_payload_size)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetPayloadSize);
    messageAppendParameter(&self->txMsg, new_payload_size);
    prvCommunicate(self);
}

unsigned int NrfusbGetPayloadSize(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetPayloadSize);
    prvCommunicate(self);
    return self->rxMsg.data[2];
}

unsigned int NrfusbGetDynamicPayloadSize(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetDynamicPayloadSize);
    prvCommunicate(self);
    return self->rxMsg.data[2];
}

void NrfusbEnableAckPayload(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdEnableAckPayload);
    prvCommunicate(self);
}

void NrfusbDisableAckPayload(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdDisableAckPayload);
    prvCommunicate(self);
}

void NrfusbEnableDynamicPayloads(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdEnableDynamicPayloads);
    prvCommunicate(self);
}

void NrfusbDisableDynamicPayloads(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdDisableDynamicPayloads);
    prvCommunicate(self);
}

void NrfusbEnableDynamicAck(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdEnableDynamicAck);
    prvCommunicate(self);
}

bool NrfusbIsPVariant(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdIsPVariant);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

void NrfusbSetAutoAck(const std::shared_ptr<NrfusbImpl>& self, bool enable)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetAutoAck1);
    messageAppendParameter(&self->txMsg, u8(enable));
    prvCommunicate(self);
}

void NrfusbSetAutoAck(const std::shared_ptr<NrfusbImpl>& self, unsigned int pipe_number, bool enable)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetAutoAck2);
    messageAppendParameter(&self->txMsg, pipe_number);
    messageAppendParameter(&self->txMsg, u8(enable));
    prvCommunicate(self);
}

void NrfusbSetPALevel(const std::shared_ptr<NrfusbImpl>& self, rf24_pa_dbm_e level, bool lnaEnable)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetPALevel);
    messageAppendParameter(&self->txMsg, u8(level));
    messageAppendParameter(&self->txMsg, u8(lnaEnable));
    prvCommunicate(self);
}

rf24_pa_dbm_e NrfusbGetPALevel(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetPALevel);
    prvCommunicate(self);
    return static_cast<rf24_pa_dbm_e>(self->rxMsg.data[2]);
}

unsigned int NrfusbGetARC(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetARC);
    prvCommunicate(self);
    return self->rxMsg.data[2];
}

bool NrfusbSetDataRate(const std::shared_ptr<NrfusbImpl>& self, rf24_datarate_e new_config)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetDataRate);
    messageAppendParameter(&self->txMsg, u8(new_config));
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

rf24_datarate_e NrfusbGetDataRate(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetDataRate);
    prvCommunicate(self);
    return static_cast<rf24_datarate_e>(self->rxMsg.data[2]);
}

void NrfusbSetCRCLength(const std::shared_ptr<NrfusbImpl>& self, rf24_crclength_e length)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetCRCLength);
    messageAppendParameter(&self->txMsg, u8(length));
    prvCommunicate(self);
}

rf24_crclength_e NrfusbGetCRCLength(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdGetCRCLength);
    prvCommunicate(self);
    return static_cast<rf24_crclength_e>(self->rxMsg.data[2]);
}

void NrfusbDisableCRC(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdDisableCRC);
    prvCommunicate(self);
}

void NrfusbMaskIRQ(const std::shared_ptr<NrfusbImpl>& self, bool tx_ok, bool tx_fail, bool rx_ready)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdMaskIRQ);
    messageAppendParameter(&self->txMsg, u8(tx_ok));
    messageAppendParameter(&self->txMsg, u8(tx_fail));
    messageAppendParameter(&self->txMsg, u8(rx_ready));
    prvCommunicate(self);
}

void NrfusbStartConstCarrier(const std::shared_ptr<NrfusbImpl>& self, rf24_pa_dbm_e level, unsigned int channel)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdStartConstCarrier);
    messageAppendParameter(&self->txMsg, u8(level));
    messageAppendParameter(&self->txMsg, channel);
    prvCommunicate(self);
}

void NrfusbStopConstCarrier(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, ansStopConstCarrier);
    prvCommunicate(self);
}

void NrfusbToggleAllPipes(const std::shared_ptr<NrfusbImpl>& self, bool isEnabled)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdToggleAllPipes);
    messageAppendParameter(&self->txMsg, u8(isEnabled));
    prvCommunicate(self);
}

void NrfusbSetRadiation(const std::shared_ptr<NrfusbImpl>& self, rf24_pa_dbm_e level, rf24_datarate_e speed, bool lnaEnable)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdToggleAllPipes);
    messageAppendParameter(&self->txMsg, u8(level));
    messageAppendParameter(&self->txMsg, u8(speed));
    messageAppendParameter(&self->txMsg, u8(lnaEnable));
    prvCommunicate(self);
}

void NrfusbLedRxOn(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetRxLedOn);
    prvCommunicate(self);
}

void NrfusbLedRxOff(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetRxLedOff);
    prvCommunicate(self);
}

void NrfusbLedTxOn(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetTxLedOn);
    prvCommunicate(self);
}

void NrfusbLedTxOff(const std::shared_ptr<NrfusbImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdSetTxLedOff);
    prvCommunicate(self);
}

bool NrfusbComboWrite(const std::shared_ptr<NrfusbImpl>& self, const void *buf, unsigned int len, bool multicast)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    messageCreate(&self->txMsg, cmdComboWrite);
    messageAppendParameter(&self->txMsg, len);
    messageAppendParameter(&self->txMsg, u8(multicast));
    messageAppendParameters(&self->txMsg, buf, len);
    prvCommunicate(self);
    return static_cast<bool>(self->rxMsg.data[2]);
}

std::shared_ptr<NrfusbImpl> NrfusbInit(const std::string& boardAddress, const unsigned int timeoutMs, const unsigned int maxRetries, std::function<void(bool, bool, bool)> userCallback)
{
    // Reserve memory to the NrfusbImpl object
    std::shared_ptr<NrfusbImpl> self = std::make_shared<NrfusbImpl>();

    // Reserve memory to the rxBuffer
    self->rxBuffer= new uint8_t[BUFFER_SIZE];

    // Initialize the asio objects
    self->asioContext = new asio::io_context();
    self->serialPort = new asio::serial_port(*self->asioContext);
    self->mutex = new std::mutex();

    // Initialize the asio thread
    self->running = true;
    self->threadAsio = std::thread(prvThreadRun, self);

    // Initialize or reserve memory for the other variables
    receiverInit(&self->usbReceiver, &self->rxMsg);
    self->waitingRx = false;
    self->waitingTx = false;
    self->timeoutMs = timeoutMs;
    self->maxRetries = maxRetries;
    self->userCallback = userCallback;

#ifdef WIN32
    // Fix Windows problem with low resolution timers, see
    // https://randomascii.wordpress.com/2020/10/04/windows-timer-resolution-the-great-rule-change
    // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FTime%2FNtSetTimerResolution.html
    // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FTime%2FNtQueryTimerResolution.html
    // https://learn.microsoft.com/en-us/windows/win32/api/timeapi/nf-timeapi-timebeginperiod
    // https://stackoverflow.com/questions/3141556/how-to-setup-timer-resolution-to-0-5-ms
    // https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-exsettimerresolution
    ULONG MinRes, MaxRes;
    NtQueryTimerResolution(&MinRes, &MaxRes, &self->oldResolution);
    if (self->oldResolution != MinRes)
    {
        NtSetTimerResolution(MaxRes, TRUE, &self->oldResolution);
    }
#endif

    // Configure the serial port
    self->serialPort->open(boardAddress);
    if (self->serialPort->is_open())
    {
#ifdef WIN32
        // In Microsoft Windows we need to add a few more options to decrease the latency
        // documentation available at:
        // https://docs.microsoft.com/pt-br/windows/win32/api/winbase/ns-winbase-commtimeouts?redirectedfrom=MSDN#remarks
        // and
        // https://stackoverflow.com/questions/10718693/set-low-latency-flag-on-a-serial-port-on-windows-in-c
        // with code example at:
        // https://valelab4.ucsf.edu/svn/micromanager2/trunk/DeviceAdapters/SerialManager/SerialManager.cpp
        auto portHandle = self->serialPort->native_handle();
        COMMTIMEOUTS timeouts;
        memset(&timeouts, 0, sizeof(timeouts));
        timeouts.ReadIntervalTimeout = MAXDWORD;
        timeouts.ReadTotalTimeoutMultiplier = MAXDWORD;
        timeouts.WriteTotalTimeoutConstant = 1;
        //timeouts.ReadIntervalTimeout = 1;
        SetCommTimeouts(portHandle, &timeouts);
#endif
    }

    return self;
}

void NrfusbDestructor(const std::shared_ptr<NrfusbImpl>& self)
{
#if WIN32
    ULONG MinRes, MaxRes, CurrentRes;
    NtQueryTimerResolution(&MinRes, &MaxRes, &CurrentRes);
    if (self->oldResolution != CurrentRes)
    {
        NtSetTimerResolution(self->oldResolution, TRUE, &CurrentRes);
    }
#endif

    self->running = false;
    if (self->serialPort->is_open())
    {
        self->serialPort->cancel();
        self->serialPort->close();
    }

    /*
     * Stop asio context (and wait for it).
     */
    self->asioContext->stop();
    while (!self->asioContext->stopped()) {}
    self->threadAsio.join();
}
