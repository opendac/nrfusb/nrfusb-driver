#ifndef NRFUSB_FIRMWARE_RECEIVER_H
#define NRFUSB_FIRMWARE_RECEIVER_H

#include "message.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum receiverState {
    receiverStateLookingForType = 0,
    receiverStateLookingForLength,
    receiverStateReceivingPayload,
    receiverStateFinished
} receiverState_t;

typedef struct receiver {
    message_t *message;
    receiverState_t state;
    uint8_t remainingBytes;
} receiver_t;

extern void receiverInit(receiver_t* self, message_t* message);
extern void receiverProcess(receiver_t *self, uint8_t newByte);
extern bool receiverHasNewMessage(receiver_t *self);
extern void receiverReset(receiver_t *self);

#ifdef __cplusplus
}
#endif

#endif //NRFUSB_FIRMWARE_RECEIVER_H
